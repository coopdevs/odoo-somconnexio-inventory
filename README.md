# Som Connexió ansible inventories

This repository stores hosts informations and related variables for this specific instance of Odoo.

## Requirements

1. Clone this repo and [odoo-provisioning](https://gitlab.com/coopdevs/odoo-provisioning) in the same directory
2. If you want to test this set up locally, install [devenv](https://github.com/coopdevs/devenv/) and do:
   ```sh
   cd odoo-somconnexio-inventory
   devenv # this creates the lxc container and sets its hostname
   ```
3. Go to `odoo-provisioning` directory and install its Ansible dependencies:
   ```sh
   ansible-galaxy install -r requirements.yml
   ```
4. Run `ansible-playbook` command pointing to the `inventory/hosts` file of this repository:
   * development local mode
   ```sh
   # tell it to keep it local with limit=dev
   # use the user root the first time to create the other users: --user=root
   ansible-playbook playbooks/sys_admins.yml -i ../odoo-somconnexio-inventory/inventory/hosts --ask-vault-pass --limit=dev
   ansible-playbook playbooks/provision.yml -i ../odoo-somconnexio-inventory/inventory/hosts --ask-vault-pass --limit=dev
   ```

> Password in BW: `Odoo Development SC - provisioning secret`

   * staging mode
   ```sh
   ansible-playbook playbooks/sys_admins.yml -i ../odoo-somconnexio-inventory/inventory/hosts --ask-vault-pass --limit=staging
   ansible-playbook playbooks/provision.yml -i ../odoo-somconnexio-inventory/inventory/hosts --ask-vault-pass --limit=staging
   ```
> Password in BW: `Odoo Staging SC - provisioning secret`

   * preprod mode
   ```sh
   ansible-playbook playbooks/sys_admins.yml -i ../odoo-somconnexio-inventory/inventory/hosts --ask-vault-pass --limit=preprod
   ansible-playbook playbooks/provision.yml -i ../odoo-somconnexio-inventory/inventory/hosts --ask-vault-pass --limit=preprod
   ```
> Password in BW: `Odoo Staging SC - provisioning secret`

   * Production mode
   ```sh
   ansible-playbook playbooks/sys_admins.yml -i ../odoo-somconnexio-inventory/inventory/hosts --ask-vault-pass --limit=production
   ansible-playbook playbooks/provision.yml -i ../odoo-somconnexio-inventory/inventory/hosts --ask-vault-pass --limit=production
   ```
> Password in BW: `Odoo SC - provisioning secret`

5. Access to the server with the browser:

* In development visit http://odoo-sc.local:8069
* In staging visit https://staging-odoo.somconnexio.coop
* In pre-production visit https://sc-preprod-odoo.coopdevs.org
* In production visit https://odoo.somconnexio.coop

## Configurate a new DB instance

1. Set admin password (in BW)
2. Set API-key in Settings/Technical/Auth Api Key (also in BW)
3. Archive (with _Action_ button) the following journals in Invoicing/Configuration/Accounting - Journals
   - Customer Invoices
   - Vendor Bills
   - Stock Journal
   - Exchange Difference
   - Bank
   - Cash
   - Cash Basis Tax Journal
4. Set `res.partner` and `contract.contract` database sequence init numbers. In debug mode, Settings/Technical/Sequences & Identifiers - Sequences. (ex. value to avoid conflict with migrated SC contracts & partners in production: 600000)

## Substitute staging DB with a backup from production

The ODOO production database keeps growing and we might need that up-to-date data in the staging server to be able to test on it. To do so, we need to follow these steps:

1. **Export a backup of the ODOO database in production**
   - Go to https://odoo.somconnexio.coop
   - Close session
   - At the bottom the login page, click on "Manage databases".
   ![alt text](files/manage_db_page.png "Manage database page")

   - Create a Backup of the current DB and download it with zip format (includes firestore)

   - For big DB, it is more appropiate to [download them with curl](#download-db-with-curl), because the UI method might result in a corrupted zipped DB.

2. **Delete the ODOO database in staging**
   We need to replace the staging database with another one, without changing its name ("odoo"). Therefore, we can't directly restore the production database into the staging server with another name, we need to delete the staging one first.
   - Go to https://staging-odoo.somconnexio.coop and acces the "Manage databases" from the login page
   - (Recommended) Download a backup copy of the staging's database before deleting it.
   - Delete the current odoo database (see [Problems deleting DB](#problems-deleting-database) in case of errors)

3. **Restore the downloaded database in the staging server**

   - Click on "Restore database" and load the exported production database in zip format. Set the same name (ex: "odoo") and state that it is a copy.

   ![alt text](files/restore_db.png "Restore database" )

   - [Verify that the size of the database suits the ngnix capacity](#update-nginx-maximum-body-size-allowed) in case of errors

4. **Post DB migration tasks**
   1. Deactivate SMTP to block outgoing mails from staging.
   Logged as admin, go to `Technical > Outgoing Mail Servers`. Archive Pangea's STMP (should be the only one).

   _Alert_: a firewall rule also blocks the outgoing mails from port 587:

   ```
   -A OUTPUT -p tcp --dport 587 -j DROP"
   ```

   Testing servers that do not require outgoing mails to be send should always have this line within the iptable lines (block Chain OUTPUT):

   ```
   ssh <user>@staging-odoo.somconnexio.coop
   <server> sudo iptables -L
   ```

   2. Deactivate IMAP/POP3 servers to block incoming mails from staging.
   Logged as admin, go to `Technical > Ingoing Mail Servers`. Archive POP server (should be the only one).

   3. Put back the test-environment ribbon.
   Logged as admin, go to `Technical > System Parameters` and set a name to `ribbon.name`. Restart the page.

   ![alt text](files/test_ribbon.png "Test environment ribbon" )

   4. Update admin's password. After migrating the DB, the admin's passoword for staging server will be the one brought from the productive server. Login is as admin, go to the profile's preferences, and update the password, restoring the original staging one.

   5. (Recommended) Stop the odoo service, update the DB with the present odoo-somconnexio module code, and start it back.

   ```
   ssh <user>@staging-odoo.somconnexio.coop
   <server> sudo su odoo
   <server> cd /opt/odoo
   <server> pyenv activate odoo
   <server> sudo systemctl stop odoo
   <server> ./odoo-bin -c /etc/odoo/odoo.conf -u somconnexio -d odoo --stop-after-init --logfile /dev/stdout
   <server> sudo systemctl start odoo
   ```

### **Download DB with curl**

 1. Enter into the Odoo server from which we want the DB backup.
 2. Check that there is enough GB space (>20GB, usually) on the filesystem mounted on the base path ("/")
   ```
   df -h
   ```

 3. Execute the following curl from inside the server (will take some minutes):

   ```
curl -X POST \
       -F "name=<DB_NAME>" \
       -F "backup_format=zip" \
       -F "master_pwd=<DB_MASTER_PASSWORD>"
       -F "master_pwd=foobar1234" \
       -o "<USER_HOME>/odoo-backup-$(date +%Y-%m-%d).zip" \
       --verbose --ipv4 \
       http://localhost:8069/web/database/backup
   ```

   - <DB_MASTER_PASSWORD> can be found here:

      ```
      grep "admin_passwd = " /etc/odoo/odoo.conf
      ```

   - <USER_HOME> corresponds to the path from the user executing the script (ex: /home/john)

> In production you can use the `download_db.sh` script found in `/home/odoo`: Execute `source /home/odoo/download_db.sh` with your personal user to download a ZIP in your home.

 4. Get the zip formatted DB backup from the Odoo server into your local, so it can be restored to another server.

   ```
   scp <USER>@odoo.somconnexio.coop:<USER_HOME>/odoo-backup.zip .
   ```


### **Problems deleting database**

If the page shows an "Internal Server Error" and we find this in the odoo logs:

```
2022-01-21 15:03:28,457 2411534 ERROR None odoo.sql_db: bad query: SELECT pg_terminate_backend(pid)
                      FROM pg_stat_activity
                      WHERE datname = 'odoo' AND
                            pid != pg_backend_pid()
ERROR: must be a member of the role whose process is being terminated or member of pg_signal_backend
```

We need to force the termination from active database connections. To do so, we must connect as database superuser.

```sh
sudo su postgres
psql
> postgres=# select pg_terminate_backend (pid) from pg_stat_activity where pg_stat_activity.datname = 'odoo';
```

Then, we can try again to delete the database from the UI or with a `dropdb odoo` command.


### **Update NGINX maximum body size allowed**

NGINX has to allow to load files with such capacity that the size of the new database fits in. We can modify this size limit by accessing the configuration file:

```sh
sudo vim /etc/nginx/sites-enabled/odoo.ssl.conf
```

And uptating the `client_max_body_size` parameter within the server ones:

```
server {
   ...
   client_max_body_size 2G;
   ...
}
```

Finally, we need to reload the service to apply the change.

```
sudo systemctl reload nginx.service
```

After correctly loading the big-sized DB, we recommend to undo the changes in the nginx configuration file (so it corresponds with the provisioned version) and reload the service again.

## Instances

* [Staging Odoo Som Connexió](https://staging-odoo.somconnexio.coop)
* [PreProduction Odoo Som Connexió](https://sc-preprod-odoo.coopdevs.org)

## Development

Follow the instructions in the SomConnexio module:

https://gitlab.com/coopdevs/odoo-somconnexio/-/tree/master/somconnexio#development
